# README #

You have just downloaded all your emails into mbox format. You may have done this from thunderbird or mac-mail or even from Outlook using TechHit MessageSave. With thousands of mbox files representing years and years of business, how do you upload it to fastmail ?

This is one solution.
 
### What is this repository for? ###

* From the filename of the mbox file, a folder structure is derived and that folder tree is re-created on the IMAP server.
    * example: if file is folder1#folder2#folder3.mbox, folder1 > folder2 > folder3 is created on the IMAP server
* Upload mbox files one by one to fastmail or another IMAP server to the newly created folder tree.
* Version 1

### How do I get set up? ###

* Summary of set up
    1. Place the python files in the same directory
    2. Edit mbox-upload.py and change the following entries:
        * '--port=992',
        * '--user=I_love@fastmail.com',
        * '--password=this-is-my-password',
        * '--host=mail.messagingengine.com'
        * This is the folder containing all your mbox files: 
            * folder_name=u'/Volumes/Install OS X Yosemite/emails'
        * This is the folder where the mbox file will be moved to after uploading:
            * finished_folder_name=u'/Volumes/Install OS X Yosemite/uploaded'
    3. type in the command line:
        * python mbox-upload.py
* Dependencies
    * tested on accounts on fastmail.com
    * tested on python 2.7 on Mac Yosemite