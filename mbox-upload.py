# -*- coding: utf-8 -*-
# https://docs.python.org/2/library/os.html#files-and-directories
# https://docs.python.org/2/library/os.path.html?highlight=path#module-os.path
# https://docs.python.org/2/library/shutil.html?highlight=shutil#module-shutil
# http://www.regular-expressions.info/characters.html#special
import config
import os, errno
import shutil
import re
import imap_upload
import imap_worker


#https://docs.python.org/2/howto/unicode.html#unicode-literals-in-python-source-code
#http://stackoverflow.com/questions/28385673/special-character-with-chinese-characters-not-substituted-in-python-string/28385712#28385712
if __name__ == "__main__":
    folder_name=u'/Volumes/Install OS X Yosemite/emails'
    finished_folder_name=u'/Volumes/Install OS X Yosemite/uploaded'

    (options,err)=imap_worker.get_options(['--ssl','--port=992','--user=I_love@fastmail.com','--password=this-is-my-password','--host=mail.messagingengine.com','xxxx.mbox'])
    if err==0:
        options_copy=options.copy()
        (uploader,err)=imap_worker.imap_login(options_copy)


    filenames=os.listdir(folder_name)
    if '.DS_Store' in filenames:
        filenames.remove('.DS_Store')
    for f in filenames:
        sourcename=os.path.join(folder_name,f)
        finishedfilename=os.path.join(finished_folder_name,f)
        f1=os.path.splitext(f) # take off extension
        # replace all bad characters with space
        ## these are ok $'+,-&0123456789:=@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz~
        precleaned=re.sub(ur'[\\^$.|?&*+\(\)\[\]\{\};）（；]',' ',f1[0])
        # replace multiple white space with just one white space
        cleaned=' '.join(precleaned.split())
        # take out "Inbox#"
        if cleaned.startswith('Inbox#'):
            cleaned=cleaned.replace('Inbox#','')
        # split into a folder tree
        path_tree=cleaned.split('#')
        folderX=''
        for ixp in path_tree[:len(path_tree)-1]:
            folderX=os.path.join(folderX,ixp.strip())
        destname=os.path.join(folderX,path_tree[-1].strip())
        # convert to Imap folder path
        mbox_name=destname.replace(r'/','.')
        # take out space because of difficulty to create folders with space
        mbox_name=mbox_name.replace(ur' ','_')
        uploader.box=''+mbox_name
        print >>imap_upload.sys.stderr,'source filename=',sourcename
        print >>imap_upload.sys.stderr,'imap folder structure=',uploader.box
        if config.IMAPSERVER:
            imap_worker.create_imap_folder(uploader)
            options_copy=options.copy()
            imap_worker.mainupload(uploader,options_copy,sourcename)
            shutil.move(sourcename,finishedfilename)

    if config.IMAPSERVER:
        uploader.close()

