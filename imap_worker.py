# -*- coding: utf-8 -*-
import config
import imap_upload

def dummy():
    return
    

def get_options(args=None):
    parser = imap_upload.MyOptionParser()
    options = dummy()
    try:
        # Setup locale
        # Set LC_TIME to "C" so that imaplib.Time2Internaldate() 
        # uses English month name.
        imap_upload.locale.setlocale(imap_upload.locale.LC_ALL, "")
        imap_upload.locale.setlocale(imap_upload.locale.LC_TIME, "C")
        #  Encoding of the sys.stderr
        enc = imap_upload.locale.getlocale()[1] or "utf_8"
        imap_upload.sys.stderr = imap_upload.codecs.lookup(enc)[-1](imap_upload.sys.stderr, errors="ignore")

        # Parse arguments
        if args is None:
            args = imap_upload.sys.argv[1:]
        options = parser.parse_args(args)
        if len(str(options.user)) == 0:
            print "User name: ",
            options.user = imap_upload.sys.stdin.readline().rstrip("\n")
        if len(str(options.password)) == 0:
            options.password = getpass.getpass()
        options = options.__dict__
        return (options,0)
    except imap_upload.optparse.OptParseError, e:
        print >>imap_upload.sys.stderr, e
        return (options,2)
    except Exception, e:
        print >>imap_upload.sys.stderr, "An unknown error has occurred: ", e
        return (options,1)

def imap_login(options):
        # Connect to the server and login
    try:
        src = options.pop("src")
        err = options.pop("error")
        time_fields = options.pop("time_fields")
        print >>imap_upload.sys.stderr, \
              "Connecting to %s:%s." % (options["host"], options["port"])
        uploader = imap_upload.IMAPUploader(**options)
        if config.IMAPSERVER:
            uploader.open()
        return (uploader,0)
    except imap_upload.socket.timeout, e:
        print >>imap_upload.sys.stderr, "Timed out"
        return (uploader,1)
    except imap_upload.imaplib.IMAP4.error, e:
        print >>imap_upload.sys.stderr, "IMAP4 error:", e
        return (uploader,1)

def create_imap_folder(uploader):
        # Prepare source and error mbox
        path_tree=uploader.box.split('.')
        folderX=''
        for ixp in path_tree:
            folderX='.'.join([folderX,ixp.strip()])
            folderY=folderX.replace('.','',1)
            print >>imap_upload.sys.stderr, "Creating sub-folder:",folderY
            if config.IMAPSERVER:
                uploader.create(folderY)
        return

def mainupload(uploader,options,src):
    try:
        tmp = options.pop("src")
        err = options.pop("error")
        time_fields = options.pop("time_fields")
        # Prepare source and error mbox
        src = imap_upload.mailbox.mbox(src, create=False)
        if err:
            err = imap_upload.mailbox.mbox(err)
        # Upload
        print >>imap_upload.sys.stderr, "Uploading..."
        imap_upload.upload(uploader, src, err, time_fields)
        return 0
    except imap_upload.mailbox.NoSuchMailboxError, e:
        print >>imap_upload.sys.stderr, "No such mailbox:", e
        return 1
    except imap_upload.socket.timeout, e:
        print >>imap_upload.sys.stderr, "Timed out"
        return 1
    except imap_upload.imaplib.IMAP4.error, e:
        print >>imap_upload.sys.stderr, "IMAP4 error:", e
        return 1
    except KeyboardInterrupt, e:
        print >>imap_upload.sys.stderr, "Interrupted"
        return 130
    except Exception, e:
        print >>imap_upload.sys.stderr, "An unknown error has occurred: ", e
        return 1

